# JavaJDBC4

Ejemplo de programación con **JDBC** con rollback y pool de conexiones en Java 18, con las librerías de maven.

## Requisitos

* JDK 18 ó superior.
* MySQL server 5.1 ó superior.
* Librerias de Maven.

***

## Instalación
* Compile el proyecto con el IDE de su preferencia.
* Ejecute desde la consola; en la raíz del proyecto, java Ejecutable.jar 

## Authors and acknowledgment
[GitLab: alfaCentauri1](https://gitlab.com/alfaCentauri1)

## License
GNU version 3.

## Project status
* Developer