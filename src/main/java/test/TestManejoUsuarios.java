package test;

import datos.Conexion;
import datos.UsuarioDAO;
import domain.Usuario;

import java.sql.Connection;
import java.sql.SQLException;

public class TestManejoUsuarios {
    public static void main(String[] args) {
        Connection conexion = null;
        try {
            conexion = Conexion.getConnection();
            if (conexion.getAutoCommit()) {
                conexion.setAutoCommit(false);
            }
            UsuarioDAO usuarioDAO = new UsuarioDAO();
            Usuario nuevo = new Usuario("tester1", "123456");
            Usuario nuevo2 = new Usuario("tester2", "1234567890");
            Usuario nuevo3 = new Usuario("tester3 ejemplo de falla en commit usar rollback", "123456");
            //Save
            usuarioDAO.insertar(nuevo);
            usuarioDAO.insertar(nuevo2);
            usuarioDAO.insertar(nuevo3);
            //Update
            nuevo2.setPassword("test123");
            usuarioDAO.actualizar(nuevo2);
            //Delete
            usuarioDAO.eliminar(3);
            //Show
            usuarioDAO.listar();
            //
            usuarioDAO.closeConection();
        }catch (SQLException exception){
            exception.printStackTrace(System.out);
            System.out.println("Entramos en un rollback");
            try {
                conexion.rollback();
            } catch (SQLException e) {
                System.out.println("Fallo el rollback: " + e.getMessage());
            }
        }
    }
}
