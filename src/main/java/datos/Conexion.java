package datos;

import org.apache.commons.dbcp2.BasicDataSource;
import javax.sql.DataSource;
import java.sql.*;

public class Conexion {
    private static final String JDBC_URL = "jdbc:mysql://localhost:3306/javaTest?useSSL=false&useTimezone=true&serverTimezone=UTC&allowPublicKeyRetrieval=true";
    private static final String JDBC_USER = "usuarioTest";
    private static final String JDBC_PASSWORD = "tu_clave";

    public static void close(ResultSet resultado) throws SQLException {
        resultado.close();
    }

    public static void close(Statement instruccion) throws SQLException {
        instruccion.close();
    }

    public static void close(PreparedStatement smtm) throws SQLException {
        smtm.close();
    }

    public static void close(Connection conexion) throws SQLException {
        conexion.close();
    }

    public static Connection getConnection() throws SQLException {
        return getDataSource().getConnection();
    }

    public static DataSource getDataSource(){
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setUrl(JDBC_URL);
        dataSource.setUsername(JDBC_USER);
        dataSource.setPassword(JDBC_PASSWORD);
        //Definición del tamaño inicial del pool de conexiones
        dataSource.setInitialSize(2);
        return dataSource;
    }
}
