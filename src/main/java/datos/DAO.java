package datos;

import java.sql.*;

public abstract class DAO implements ITypeDAO{
    protected Connection conexionTransactional;
    protected PreparedStatement preparedStatement;
    protected ResultSet resultado;
}
